#include <iostream>
#include "SDL/SDL.h"
using namespace std;

class Complex
{
    long double re;
    long double im;
public:
    Complex (long double a, long double b): re(a), im(b){}
    Complex (const Complex &comp) : re(comp.re), im(comp.im) {}

    void SetX (long double x)
    {
        re = x;
    }

    void SetY (long double y)
    {
        im = y;
    }

    Complex & operator=(const Complex &num)
    {
        re = num.re;
        im = num.im;
        return *this;
    }

    Complex operator+(const Complex &num)
    {
        Complex result(0,0);
        result.re = re + num.re;
        result.im = im + num.im;
        return result;
    }

    Complex operator*(const Complex &num)
    {
        Complex result(0,0);
        result.re = re * num.re - im * num.im;
        result.im = re * num.im + im * num.re;
        return result;
    }

    friend istream & operator>> (istream &stream, Complex &c);
    void input()
    {
        cin >> re >> im;
    }
    long double abs2 ()
    {
        return re * re + im * im;
    }
};

int Mandelbrot_it (Complex &c, int k)
{
    Complex temp (0,0);
    int i;
    for (i = 0; i <= k; i++)
    {
        temp = temp * temp + c;
        if (temp.abs2() > 4)
        {
            if (i)
                return i;
            else
                return 1;
        }
    }
    return 0;
}

istream & operator>> (istream &stream, Complex &c)
{
    stream >> c.re >> c.im;
    return stream;
}

void DrawPixel(SDL_Surface *screen, int x, int y, Uint8 R, Uint8 G, Uint8 B){

 Uint32 color = SDL_MapRGB(screen->format, R, G, B);
 switch (screen->format->BytesPerPixel){
   case 1:  // Assuming 8-bpp
   {
     Uint8 *bufp;
     bufp = (Uint8 *)screen->pixels + y*screen->pitch + x; *bufp = color;
   } break;
   case 2: // Probably 15-bpp or 16-bpp
   {
     Uint16 *bufp;
     bufp = (Uint16 *)screen->pixels + y*screen->pitch/2 + x; *bufp = color;
   } break;
   case 3: // Slow 24-bpp mode, usually not used
   {
     Uint8 *bufp;
     bufp = (Uint8 *)screen->pixels + y*screen->pitch + x * 3;
     if(SDL_BYTEORDER == SDL_LIL_ENDIAN){
       bufp[0] = color;
       bufp[1] = color >> 8;
       bufp[2] = color >> 16;
     }else{
       bufp[2] = color;
       bufp[1] = color >> 8;
       bufp[0] = color >> 16;
     }
   } break;
   case 4: // Probably 32-bpp
   {
     Uint32 *bufp;
     bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
     *bufp = color;
   } break;
 }
}

void Slock(SDL_Surface *screen){

 if ( SDL_MUSTLOCK(screen) ){
   if ( SDL_LockSurface(screen) < 0 ){
     return;
   }
 }
}


void Sulock(SDL_Surface *screen){

 if ( SDL_MUSTLOCK(screen) ){
   SDL_UnlockSurface(screen);
 }
}



#define     ITERATIONS      1000
#define     MoveSpeed       30
#define     ColourGrad      30
#define     StartSize       2.5
#define     ScreenWidth     600
#define     ScreenHeigth    480
#define     Bpp             32
#define     HotR            255
#define     HotG            107
#define     HotB            0
#define     ColdR           0
#define     ColdG           0
#define     ColdB           0
// [255,107,0] = Orange
// [107,10,171] = Purple
int main(int argc, char **argv)
{
    Complex c(0,0);
    SDL_Surface *Screen;
    SDL_Event Event;
    bool MovingXP = false;
    bool MovingXN = false;
    bool MovingYP = false;
    bool MovingYN = false;
    bool Running = true;
    bool UpToDate = false;
    bool Zooming = true;
    int Isnt = 0;
    int OffsetX = ScreenWidth / 2;
    int OffsetY = ScreenHeigth / 2;
    int XFrameB = (ScreenWidth - ScreenHeigth) / 2;
    int XFrameE = (ScreenWidth + ScreenHeigth) / 2;
    double Coef = 0.009765625 ;
    int Zoom = 1;

    SDL_WM_SetCaption("Mandelbrot Set","ex2");
    if (SDL_Init (SDL_INIT_EVERYTHING) < 0)
    {
        cout << "Unable to init SDL: " << SDL_GetError();
        return 1;
    }

    Screen = SDL_SetVideoMode(ScreenWidth,ScreenHeigth,Bpp, SDL_SWSURFACE | SDL_DOUBLEBUF );
    if (Screen == NULL)
    {
        cout << "Unable to set video: " << SDL_GetError();
        return 1;
    }
    while (Running)
    {
        if (!UpToDate)
        {
        if (Zooming)
        {
            Coef = (StartSize / double (Zoom) ) / OffsetY;
            Zooming = false;
        }
        Slock (Screen);
        for (int x = XFrameB; x < XFrameE ; x++)
        {
            for (int y = 0; y < ScreenHeigth; y++)
            {
                c.SetX( double (x - OffsetX) * Coef );
                c.SetY( double (y - OffsetY) * Coef * (-1) );
                Isnt = Mandelbrot_it (c,ITERATIONS);
                if (Isnt)
                {
                    int j = ColourGrad * Isnt;
                    DrawPixel
                    (   Screen, x, y,
                        ColdR + j * (HotR - ColdR) / ITERATIONS,
                        ColdG + j * (HotG - ColdG) / ITERATIONS,
                        ColdB + j * (HotB - ColdB) / ITERATIONS
                    );
                }
                else
                    DrawPixel (Screen, x, y, 255, 255, 255);
            }
        }
        Sulock (Screen);
        SDL_Flip (Screen);
        UpToDate = true;
        }
        while (SDL_PollEvent(&Event))
        {
            if (Event.type == SDL_QUIT ) { Running = false; }
            if (Event.type == SDL_KEYDOWN)
                switch(Event.key.keysym.sym)
                {
                    case (SDLK_i):
                    {
                        Zoom += 1;
                        UpToDate = false;
                        Zooming = true;
                        break;
                    }
                    case (SDLK_o):
                    {
                        Zoom -= 1;
                        UpToDate = false;
                        Zooming = true;
                        break;
                    }
                    case (SDLK_ESCAPE):
                    {
                        Running = false;
                        break;
                    }
                    case (SDLK_UP):
                    {
                        MovingYP = true;
                        break;
                    }
                    case (SDLK_DOWN):
                    {
                        MovingYN = true;
                        break;
                    }
                    case (SDLK_RIGHT):
                    {
                        MovingXP = true;
                        break;
                    }
                    case (SDLK_LEFT):
                    {
                        MovingXN = true;
                        break;
                    }
                    default: break;
                }
                else
                if (Event.type == SDL_KEYUP)
                {
                    switch(Event.key.keysym.sym)
                    {
                        case (SDLK_UP):
                        {
                            MovingYP = false;
                            break;
                        }
                        case (SDLK_DOWN):
                        {
                            MovingYN = false;
                            break;
                        }
                        case (SDLK_RIGHT):
                        {
                            MovingXP = false;
                            break;
                        }
                        case (SDLK_LEFT):
                        {
                            MovingXN = false;
                            break;
                        }
                        default: break;
                    }
                }
        }
        if (MovingXP)
        {
            OffsetX -= MoveSpeed;
            UpToDate = false;
        }
        else if(MovingXN)
        {
            OffsetX += MoveSpeed;
            UpToDate = false;
        }
        if (MovingYP)
        {
            OffsetY +=MoveSpeed;
            UpToDate = false;
        }
        else if (MovingYN)
        {
            OffsetY -= MoveSpeed;
            UpToDate = false;
        }
    }
    SDL_FreeSurface(Screen);
    SDL_Quit();
    return 0;
}
